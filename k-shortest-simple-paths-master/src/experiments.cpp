
#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <cstdlib>
#include <limits>       /* To do with min degree */
#include <fstream>
#include <sstream>
#include "../include/easy_digraph.h"

using namespace std::chrono;
using namespace directed_graph;

EasyDirectedGraph<size_t, int, uint32_t> *load_digraph(std::string filename, bool verbose=true)
{
  // Read graph
  if (verbose)
    {
      std::cout << "# graph (" << filename << ") : ";
      auto start = high_resolution_clock::now();
      EasyDirectedGraph<size_t, int, uint32_t> *G = new EasyDirectedGraph<size_t, int, uint32_t>(filename);
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<milliseconds>(stop - start); // can also be cast to microseconds
      std::cout << G->n << " vertices and " << G->m << " edges : " << duration.count() << " ms\n" << std::endl;
      return G;
    }
  else
    return new EasyDirectedGraph<size_t, int, uint32_t>(filename);
}

/*
 * Build a list a x random pairs of vertices in V
 */
std::vector<std::pair<size_t, size_t> > random_pairs(std::vector<size_t> V, uint32_t x)
{
    std::ofstream myfile;
    myfile.open("./PairsPerCity/FLApairs.txt");
    
    size_t n = V.size();
    std::vector<std::pair<size_t, size_t> > pairs;
    pairs.clear();
    std::srand(std::time(nullptr));
    while (pairs.size() < x)
    {
      size_t u = n * (rand() / (RAND_MAX + 1.0));
      size_t v = u;
      while (u == v)
          v = n * (rand() / (RAND_MAX + 1.0));
      myfile <<u<<" "<<v<<std::endl;
      pairs.push_back(std::make_pair(V[u], V[v]));
      if (pairs.size() == x)
      { // remove possible duplicates
          std::sort(pairs.begin(), pairs.end());
          auto new_end = std::unique(pairs.begin(), pairs.end());
          pairs.erase(new_end, pairs.end());
      }
    }
    myfile.close();
    return pairs;
}


/*
 * Build a list pairs red from a file ...
 */
std::vector<std::pair<size_t, size_t> > read_pairs(std::string Fname, size_t nbp)
{

    std::vector<std::pair<size_t, size_t> > Vpairs;
    Vpairs.clear();
    std::pair<size_t, size_t> P;

    // Read from the text file
    // Fname = "./PairsPerCity/Romepairs.txt";
    std::ifstream MyReadFile(Fname);
    std::string a,b;
    for (size_t  i = 0; i < nbp; i++)
    {
        MyReadFile >> a >> b;
        P.first = std::stoi(a);
        P.second = std::stoi(b);
        Vpairs.push_back(P);
    }
    return Vpairs;

}




/*
 * Run specified algorithm from source to target.
 * Display timing for given list of k values
 */
void run_algo1(EasyDirectedGraph<size_t, int, uint32_t> *G,
          size_t source, size_t target,
          std::string algorithm, size_t version,
          std::vector<size_t> k_values)
{
    size_t distance, nb_hops, max_nb_hops;
    
  G->reset_kssp();
  auto start_init = high_resolution_clock::now();
  G->init_kssp(algorithm, source, target, version, false);
  auto stop_init = high_resolution_clock::now();
  auto duration_init = duration_cast<milliseconds>(stop_init - start_init);
  size_t total_duration = duration_init.count();
  
  size_t cpt = 0;
  for (auto const& k: k_values)
    {
        auto start = high_resolution_clock::now();
        while (cpt < k)
        {
            auto sol = G->next_path();
            if (cpt == 0){
                distance = sol.second;
                nb_hops = sol.first.size();
                max_nb_hops = nb_hops;
            }
            else
                if(sol.first.size() > max_nb_hops)
                    max_nb_hops = sol.first.size();
                if (sol.second == 0)
                    break;
            cpt++;
        }
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
        total_duration += duration.count();


        std::string ver;
        if (version == 1)
            ver = "";
        else if (version == 2)
            ver = "v2";
        else if (version == 3)
            ver = "v3";
        else // version == 4
            ver = "*";
        std::cout << algorithm << ver ;//<< (version == 1 ? "" : "*");
      std::cout << "\t" << source << "\t" << target << "\t" << distance << "\t" << nb_hops << "\t" << max_nb_hops << "\t"<< k;
      std::cout << "\t" << cpt; // should sway k and k*
      std::cout << "\t" << G->used_trees();
      std::cout << "\t" << total_duration;
      std::cout << std::endl;
        
      if (cpt != k)
    break;
    }
}



/*
 * For the input graph, solves the problem for
 * - SB, SB* and PSB
 * - k = 100, 200, 250, ..., 1000
 * - 1000 randomly selected pairs
 *
 */
void experience1(std::string filename) // for bigs
{
  // std::vector<std::pair<std::string, size_t> > algorithms = {{"PNC", 1},{"NC", 1},{"PNCs", 1},{"SB", 1}, {"SB", 4}, {"PSB", 1},{"PSB", 2},{"PSB", 3}};
    std::vector<std::pair<std::string, size_t> > algorithms = {{"Y", 1},{"PNC", 1},{"PNC*", 1}};

   // std::vector<size_t> ks = { 10, 20, 50, 100, 200, 500, 1000, 5000, 10000};
    std::vector<size_t> ks = {2, 5, 10,  20,  30,  50,   60,   70,   100};

  EasyDirectedGraph<size_t, int, uint32_t> *G = load_digraph(filename);
   std::vector<std::pair<size_t, size_t> > pairs = random_pairs(G->int_to_vertex, 100);
  //  std::string Fname = "./PairsPerCity/FLApairs.txt";
  //   std::vector<std::pair<size_t, size_t> > pairs = read_pairs(Fname, 10);

  std::cout << "Algo\tsource\ttarget\tdist\tnb_h\tMnb_h\tk\tk*\ttrees\ttime (ms)" <<std::endl;
  for (auto const& p: pairs)
    for (auto const& algo: algorithms)
      run_algo1(G, p.first, p.second, algo.first, algo.second, ks);
 
  delete G;
}

void experience2(std::string filename) // for smalls
{
   std::vector<std::pair<std::string, size_t> > algorithms = {{"PNC", 1},{"NC", 1},{"PNCs", 1},{"SB", 1}, {"SB", 4}, {"PSB", 1},{"PSB", 2},{"PSB", 3}};

    std::vector<size_t> ks = {2, 5,  10, 20, 50, 100, 200, 500, 1000};
  EasyDirectedGraph<size_t, int, uint32_t> *G = load_digraph(filename);
  // std::vector<std::pair<size_t, size_t> > pairs = random_pairs(G->int_to_vertex, 1000);
    std::string Fname = "./PairsPerCity/DEpairs.txt";
   // std::cout<<""<<std::endl;
    std::vector<std::pair<size_t, size_t> > pairs = read_pairs(Fname, 1000);

  std::cout << "Algo\tsource\ttarget\tdist\tnb_h\tMnb_h\tk\tk*\ttrees\ttime (ms)" <<std::endl;
  for (auto const& p: pairs)
    for (auto const& algo: algorithms)
      run_algo1(G, p.first, p.second, algo.first, algo.second, ks);
 
  delete G;
}





int main(int argc, char* argv[])
{
  // if (argc < 2)
  //   print_usage();
    /*
    std::vector<std::pair<size_t, size_t> > Vp;
    Vp.clear();
    std::string Fname = "./PairsPerCity/Romepairs.txt";
    Vp = read_pairs(Fname);
    std::cout<<"printing P"<<std::endl;
    for (size_t i = 0; i < Vp.size(); i++)
        std::cout<<" "<<Vp[i].first<<" "<<Vp[i].second<<std::endl;
    
    */
    switch (std::stoi(argv[1])) {
        case 1:
            experience1(argv[2]);
            break;
        case 2:
              experience2(argv[2]); // with Yen
              break;
          /*

  case 6:
    experience6(argv[2]);
    break;

  case 3:
    experience3(argv[2]);
    break;
  case 4:
    experience4(argv[2], atoi(argv[3]), atoi(argv[4]));
    break;
  case 5:
    experience5(argv[2], atoi(argv[3]), atoi(argv[4]));
    break;*/
        default:
            break;
  }
}
