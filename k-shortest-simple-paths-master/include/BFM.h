/*
 * Dijkstra replaced by BFM
 *
 * Implement standard Dijkstra single source shortest path algorithm.
 * By default, it build shortest path tree from specified source vertex encoded
 * with predecessor function. Setting parameter reverse to true, it builds
 * shortest path tree to target with successor function.
 */


#ifndef BFM_H
#define BFM_H

#include <cstdint>
#include <set>
#include <unordered_set>
#include <numeric>    // to get std::iota
#include <assert.h>     /* assert */
#include <limits>       /* used to get maximum possible value of type TV */
#include "digraph.h"


namespace BFM {

template<
typename TI,  // type of node indices (unsigned int, uint32_t, etc.)
typename TV   // type of edge weights (int, unsigned int, float, etc.)
>
class BFM
{
public:
    BFM() = default;
    
    // Constructor
    explicit BFM(directed_graph::DirectedGraph<TI,TV> *g,
                      const TI &source,
                      const bool &reverse);
    
    // Constructor
    explicit BFM(directed_graph::DirectedGraph<TI,TV> *g,
                      std::unordered_set<TI> forbidden_vertices,
                      std::set<std::pair<TI,TI> > forbidden_edges,
                      const TI &source,
                      const bool &reverse);
    
    // Destructor
    virtual ~BFM();
    
    // Compute shortest paths tree
    bool run();
    
    // Get distance from/to source
    TV weight(const TI &u);
    
    // Get predecessor of u in shortest path from source
    TI predecessor(const TI &u);
    
    // Get successor of u in shortest path to source
    TI successor(const TI &u);
    
    // Get path from/to u
    std::vector<TI> get_path(const TI &u);

    bool hasNegativeCycle();
    
private:
    directed_graph::DirectedGraph<TI,TV> *graph;
    TI n;
    TI source;
    std::vector<TI> _predecessor;
    std::vector<TV> _weight;
    std::vector<TI> positions;
    //TI *_predecessor;
    //TV *_weight;
    std::unordered_set<TI> f_vertices;
    std::set<std::pair<TI,TI> > f_edges;
    bool reverse;

    TV MAX_WEIGHT;

    int Has_negative_cycle; // 0 : no negative cycle, 1 : has negative cycles, 2: we don't know yet

    using VertexSet = std::unordered_set<TI>;

    // Initialize data structures
    void initialize();
    
    // Compute shortest paths from source
    int runFrom();

    void runFrom_brute();

    // Compute shortest paths to source
    int runTo();
};


// Constructor
template<typename TI, typename TV>
BFM<TI, TV>::BFM(directed_graph::DirectedGraph<TI,TV> *g,
                           const TI &source,
                           const bool &reverse):
graph(g), n(g->n), source(source), reverse(reverse)
{
    initialize();
}


// Constructor
template<typename TI, typename TV>
BFM<TI, TV>::BFM(directed_graph::DirectedGraph<TI,TV> *g,
                           std::unordered_set<TI> forbidden_vertices,
                           std::set<std::pair<TI,TI> > forbidden_edges,
                           const TI &source,
                           const bool &reverse):
graph(g), n(g->n), source(source),
f_vertices(forbidden_vertices), f_edges(forbidden_edges), reverse(reverse)
{
    initialize();
}


// Destructor
template<typename TI, typename TV>
BFM<TI, TV>::~BFM()
{
    //free(_predecessor);
    //free(_weight);
    // delete pq;
}


// Initialize data structures
template<typename TI, typename TV>
void BFM<TI, TV>::initialize()
{
    Has_negative_cycle = 2;
    MAX_WEIGHT = std::numeric_limits<TV>::max();
    _predecessor.resize(n);
    std::iota(_predecessor.begin(), _predecessor.end(), 0);  // Fill with 0, 1, ..., n-1.
    _weight.assign(n, MAX_WEIGHT);  // resize the vector and initialize to MAX_WEIGHT
    positions.assign(n ,0);  // resign the vector and initialize to 0
    _weight[source] = 0;
}


// Compute shortest paths
template<typename TI, typename TV>
bool BFM<TI, TV>::run() // return 0 if negative cycle is found, 1 otherwise
{
    // std::cout<<"hey run "<<std::endl;
    if (reverse)
        return (runTo() == 1);
    else
        //runFrom_brute();
        return (runFrom() == 1);
    //return this->hasNegativeCycle();
     //return true;
}


// Compute shortest paths from source until target is reached
template<typename TI, typename TV>
void BFM<TI, TV>::runFrom_brute()
{
    // std::cout<<" run from brute: BFM is called"<<std::endl;
    TI u,v;
    TV w;
    for (size_t i = 0; i < n ; i++){

        for (auto const  arc : graph->edges){
            u = arc.first.first;
            v = arc.first.second;
            w = arc.second;
            if ((f_vertices.count(v) > 0) || (f_edges.count(std::make_pair(u, v)) > 0)){
                continue;
            }

            if (_weight[u] != MAX_WEIGHT and  _weight[u] + w < _weight[v]){
                //if (u == 1 and v == 0)
                //    std::cout<<"update here"<<u<<v<<" "<< _weight[u] + w <<" "<<_weight[v];
                _weight[v] = _weight[u] + w;
                _predecessor[v] = u;
            }
        }
    }
    // std::cout<<" end boucle "<<std::endl;
}


// Compute shortest paths from source until target is reached
template<typename TI, typename TV>
int BFM<TI, TV>::runFrom()
{
    VertexSet current_frontier = {source};
    VertexSet next_frontier;
    while (not current_frontier.empty()){
        next_frontier.clear();
        for (auto u: current_frontier){
            for (auto edge: graph->out_neighbors[u]) {
                auto v = edge.first;
                auto w = edge.second;
                if ((f_vertices.find(v) != f_vertices.end()) || (f_edges.find(std::make_pair(u, v)) != f_edges.end()))
                    continue;
                if ((_weight[u] != MAX_WEIGHT) and (_weight[u] + w < _weight[v])) {
                    // update
                    positions[v] = positions[u] + 1;
                    if (positions[v] >= n){
                        std::cout<<"pos: "<<positions[v]<<"and n: "<<n<<std::endl;
                        std::cout<<"neg cycle"<<std::endl;
                        return 0;
                    }

                    _weight[v] = _weight[u] + w;
                    _predecessor[v] = u;
                    next_frontier.insert(v);
                }
            }
        }
        current_frontier.swap(next_frontier);
    }
    return 1;
}



// Compute shortest paths from source until target is reached
template<typename TI, typename TV>
int BFM<TI, TV>::runTo()
{
    VertexSet current_frontier = {source};
    VertexSet next_frontier;

    while (not current_frontier.empty()){
        next_frontier.clear();
        for (auto u: current_frontier){
            for (auto edge: graph->in_neighbors[u]) {
                auto v = edge.first;
                auto w = edge.second;
                if ((f_vertices.find(v) != f_vertices.end()) || (f_edges.find(std::make_pair(v, u)) != f_edges.end()))
                    continue;
                if ((_weight[u] != MAX_WEIGHT) and (_weight[u] + w < _weight[v])){
                    positions[v] = positions[u] + 1;
                    if (positions[v] >= n){
                        std::cout<<"pos: "<<positions[v]<<"and n: "<<n<<std::endl;
                        std::cout<<"neg cycle"<<std::endl;
                        return 0;
                    }
                    // update
                    _weight[v] = _weight[u] + w;
                    _predecessor[v] = u;
                    next_frontier.insert(v);
                }
            }
        }
        current_frontier.swap(next_frontier);
    }
    return 1;
}

// TODO: remove this method
template<typename TI, typename TV>
bool BFM<TI, TV>::hasNegativeCycle() { // This function could be improved
    TI u, v;
    TV w;
    for (auto const arc : graph->edges) {
        u = arc.first.first;
        v = arc.first.second;
        w = arc.second;
        if (_weight[u] != MAX_WEIGHT and _weight[u] + w < _weight[v]) {
            std::cout<<"Graph contains negative weight cycle"<<u<<" "<<v<<" "<<_weight[u]<<" "<<  w<<" " <<_weight[v]<<std::endl;
            Has_negative_cycle = 1;
            return 1; // If negative cycle is detected, simply return
        }
    }
    Has_negative_cycle = 0;
    return 0;
}



// Get distance from/to source
template<typename TI, typename TV>
inline TV BFM<TI, TV>::weight(const TI &u)
{
    return _weight[u];
}

// Get predecessor of u in shortest path from source
template<typename TI, typename TV>
inline TI BFM<TI, TV>::predecessor(const TI &u)
{
    assert(not reverse);
    return _predecessor[u];
}

// Get successor of u in shortest path to source
template<typename TI, typename TV>
inline TI BFM<TI, TV>::successor(const TI &u)
{
    assert(reverse);
    return _predecessor[u];
}

// Get path from/to u
template<typename TI, typename TV>
inline std::vector<TI> BFM<TI, TV>::get_path(const TI &u)
{
    std::vector<TI> path;
    path.clear();
    
    path.push_back(u);
    TI v = u;

    while (_predecessor[v] != v)
    {
        path.push_back(_predecessor[v]);
        v = _predecessor[v];
    }
    
    if (not reverse) {
        std::reverse(path.begin(), path.end());
    }
    return path;
}






}
#endif
