# k Shortest Simple Paths

Implementation in C++ of algorithms for computing the k shortest simple paths from a source to a destination in a weighted directed graph.
In particular, the following algorithms are available:

* **Yen** -- algorithm proposed by Yen [8,9].
* **NC** -- *Node Classification* algorithm proposed by Feng in [4].
* **PNC** -- *Postponed Node Classification* algorithm proposed by Al Zoobi, Coudert and Nisse.
* **PNC*** --  Variant of PNC proposed by Al Zoobi, Coudert and Nisse.


## Authors
- [Ali Al Zoobi](http://www-sop.inria.fr/members/Ali.Al-Zoobi)
- [David Coudert](http://www-sop.inria.fr/members/David.Coudert)
- [Nicolas Nisse](http://www-sop.inria.fr/members/Nicolas.Nisse)

## License
The code is released under the GNU General Public License, version 3, or any later version as published by the Free Software Foundation.

## How to cite

### Version 2.0

### Version 1.0
* Ali Al Zoobi, David Coudert and Nicolas Nisse. **[Space and Time Trade-Off for the k Shortest Simple Paths Problem](http://dx.doi.org/10.4230/LIPIcs.SEA.2020.18)**. 18th International Symposium on Experimental Algorithms (SEA), LIPIcs volume 160(18), Jun 2020, Catania, Italy. pp.13
* [bibtex file](https://hal.inria.fr/hal-02865918/bibtex)


## Installation and usage
This code can be used from the command line or inside your own C++ program. We also provide a small interface to use it with **[Sagemath](https://www.sagemath.org)**.


### Command line

After cloning the code, simply type 
```shell
.$ make
```
or
```shell
.$ make cpp
```


#### Graph file format
Use the [DIMACS](http://users.diag.uniroma1.it/challenge9/format.shtml#graph) file format from the [9th DIMACS Implementation Challenge on Shortest Paths](http://users.diag.uniroma1.it/challenge9/) to describe weighted directed graph.

Roughly, accepted file format is as follows:
* `c This is a comment` -- Comment line. Can appear anywhere in the file.
* `p sp n m` -- Problem line indicating if the graph has self-loops (*s*), parallel arcs (*p*) and the numbers *n* of nodes and *m* of arcs. This line is ignored in our implementation.
* `a u v w` -- Arc from *u* to *v* with weight *w*. Vertex labels *u* and *v* must be integers (without assumption on the range). Weight *w* can be either a positive integer or a positive float depending on the choosen type in the code.


**Example:** ([source](http://users.diag.uniroma1.it/challenge9/samples/sample.gr)

    c 9th DIMACS Implementation Challenge: Shortest Paths
    c http://www.dis.uniroma1.it/~challenge9
    c Sample graph file
    c
    p sp 6 8
    c graph contains 6 nodes and 8 arcs
    c node ids are numbers in 1..6
    c
    a 1 2 17
    c arc from node 1 to node 2 of weight 17
    c
    a 1 3 10
    a 2 4 2
    a 3 5 0
    a 4 3 0
    a 4 6 3
    a 5 2 0
    a 5 6 20

#### Getting help:

```shell
.$ ./build/main -h
----
USAGE: ./build/main <graph_filename> <source> <target> -k <k> -r <r> -a <algo> -a <algo> ...
----
options:
 -h: print this help and exit
 -k: number of paths to compute (default: 10)
 -r: number of repetitions to average running time (default: 1)
 -a: algorithm to run among
     Y: basic algorithm as proposed by Yen
     PNC: postponed NC proposed by Al Zoobi, Coudert and Nisse
     NC: node classification algorithm proposed by Feng
     PNC*: special postponed NC improved proposed by Al Zoobi, Coudert and Nisse
 -w: whether to display the weight of computed paths (default: false)
 -p: whether to display computed paths (default: false)
```



#### Toy example:

Asking for 20 shortest simple paths from 1 to 5 in the `toy.gr` graph using algorithms `Y`, `NC`, `PNC`, `PNC*`, `SB`, `SB*`, `PSB`, `PSBv2`, `PSBv3`.
For each algorithm, the reported values correspond to the requested number `k` of paths,
the number `k*` of found paths (either equal to `k` or to the maximum number of existing simple paths from `u` to `v`),
the number of shortest path trees that have been computed (and that are stored in memory during computation for algorithms `SB`, `SB*` and `PSB`),
and the running time in milli-seconds.


```
.$ ./build/main graphs/Dummy-networks/toy.gr 1 5 -k 20 -a Y -a NC -a PNC -a PNC* -a SB -a SB* -a PSB -a PSBv2 -a PSBv3
# graph (graphs/Dummy-networks/toy.gr) : 6 vertices and 18 edges : 4 ms

Algo	source	target	k	k*	trees	time (ms)
Y	1	5	20	11	27	0
NC	1	5	20	11	5	0
PNC	1	5	20	11	5	0
PNC*	1	5	20	11	5	0
SB	1	5	20	11	5	0
SB*	1	5	20	11	5	0
PSB	1	5	20	11	5	0
PSBv2	1	5	20	11	5	0
PSBv3	1	5	20	11	6	0
```

It is possible to get the list of weights of the paths:

```
.$ ./build/main graphs/Dummy-networks/toy.gr 1 5 -k 20 -a Y -a PSB -w
# graph (graphs/toy.gr) : 6 vertices and 18 edges : 0 ms

Algo	source	target	k	k*	trees	time (ms)
Y: path lengths = [4, 5, 6, 7, 7, 8, 8, 10, 11, 11, 11]
Y	1	5	20	11	27	0
PSB: path lengths = [4, 5, 6, 7, 7, 8, 8, 10, 11, 11, 11]
PSB*	1	5	20	11	5	0
```

and the paths:

```
.$ ./build/main graphs/Dummy-networks/toy.gr 1 5 -k 20 -a PSB -p
# graph (graphs/Dummy-networks/toy.gr) : 6 vertices and 18 edges : 0 ms

Algo	source	target	k	k*	trees	time (ms)
4	[1, 3, 2, 5]
5	[1, 2, 5]
6	[1, 3, 4, 5]
7	[1, 3, 2, 4, 5]
7	[1, 3, 4, 2, 5]
8	[1, 0, 3, 2, 5]
8	[1, 2, 4, 5]
10	[1, 0, 3, 4, 5]
11	[1, 0, 3, 2, 4, 5]
11	[1, 2, 3, 4, 5]
11	[1, 0, 3, 4, 2, 5]
PSB	1	5	20	11	5	0
.$ ./build/main graphs/Dummy-networks/toy.gr 1 5 -k 20 -a PNC -p
# graph (graphs/Dummy-networks/toy.gr) : 6 vertices and 18 edges : 0 ms

Algo	source	target	k	k*	trees	time (ms)
4	[1, 3, 2, 5]
5	[1, 2, 5]
6	[1, 3, 4, 5]
7	[1, 3, 2, 4, 5]
7	[1, 3, 4, 2, 5]
8	[1, 0, 3, 2, 5]
8	[1, 2, 4, 5]
10	[1, 0, 3, 4, 5]
11	[1, 2, 3, 4, 5]
11	[1, 0, 3, 2, 4, 5]
11	[1, 0, 3, 4, 2, 5]
PNC	1	5	20	11	5	0
```

#### Larger example:

```shell
.$ ./build/main graphs/Road-networks/ROME.gr 1 1523 -k 100 -a Y -a NC -a PNC -a SB -a SB* -a PSB
# graph (graphs/Road-networks/ROME.gr) : 3353 vertices and 8859 edges : 24 ms

Algo	source	target	k	k*	trees	time (ms)
Y	1	1523	100	100	1031	118
NC	1	1523	100	100	622	10
PNC	1	1523	100	100	83	8
SB	1	1523	100	100	74	42
SB*	1	1523	100	100	74	8
PSB	1	1523	100	100	41	17
```

### From your C++ program
You can use this code inside your own program doing something like
```c++
#include "../include/easy_digraph.h"

using namespace directed_graph;

EasyDirectedGraph<size_t, uint32_t, uint32_t> *G = new EasyDirectedGraph<size_t, uint32_t, uint32_t>("graphs/rome99.gr");
G->init_kssp("NC", 1, 1253, 1, false);
for (auto cpt=0; cpt < k; cpt++) {
    auto sol = G->next_path();
    /* ... */
}
/* ... */
G->reset_kssp();
G->init_kssp("PNC", 1, 1253, 1, false);
for (auto cpt=0; cpt < k; cpt++) {
    auto sol = G->next_path();
    /* ... */
}
/* ... */
delete G;
```

### From Sagemath
We provide a minimal interface to use our code with **[Sagemath](https://www.sagemath.org)**. It can certainly be improved.


```shell
.$ make cython
```
You can use it to load a graph from a file

```python
sage: from my_digraph import MyDiGraph
sage: D = MyDiGraph('./graphs/rome99.gr', by_weight=True)
sage: it = D.parsimonious_sidetrack_based_iterator(1, 1253)
sage: print(next(it))
([1, 22, 165, 162, 167, 164, 171, 190, 191, 336, 338, 343, 344, 340, 347, 348, 335, 515, 407, 524, 582, 589, 596, 628, 630, 648, 1217, 1218, 1237, 1239, 1252, 1253], 29789)
sage: print(next(it))
([1, 22, 165, 162, 167, 164, 171, 190, 191, 336, 338, 343, 344, 340, 347, 348, 335, 515, 407, 524, 575, 578, 594, 595, 627, 628, 630, 648, 1217, 1218, 1237, 1239, 1252, 1253], 29875)
sage: print(next(it))
([1, 22, 165, 162, 167, 164, 171, 190, 191, 336, 338, 343, 344, 340, 347, 348, 335, 515, 407, 524, 582, 589, 596, 628, 630, 648, 1217, 1218, 1237, 1239, 1252, 1257, 1258, 1253], 29939)
sage:
sage: it = D.node_classification_iterator(1, 1253)
sage: print(next(it))
([1, 22, 165, 162, 167, 164, 171, 190, 191, 336, 338, 343, 344, 340, 347, 348, 335, 515, 407, 524, 582, 589, 596, 628, 630, 648, 1217, 1218, 1237, 1239, 1252, 1253], 29789)
sage: print(next(it))
([1, 22, 165, 162, 167, 164, 171, 190, 191, 336, 338, 343, 344, 340, 347, 348, 335, 515, 407, 524, 575, 578, 594, 595, 627, 628, 630, 648, 1217, 1218, 1237, 1239, 1252, 1253], 29875)
sage: print(next(it))
([1, 22, 165, 162, 167, 164, 171, 190, 191, 336, 338, 343, 344, 340, 347, 348, 335, 515, 407, 524, 582, 589, 596, 628, 630, 648, 1217, 1218, 1237, 1239, 1252, 1257, 1258, 1253], 29939)
```

You can also give it a Sage (di)graph (unweighted in this example)
```python
sage: G = graphs.PetersenGraph()
sage: D = MyDiGraph(G)
sage: for p, w in D.node_classification_iterator(1, 5):
....:     print(w, p)
....:     
2 [1, 0, 5]
3 [1, 2, 7, 5]
3 [1, 6, 8, 5]
4 [1, 2, 3, 8, 5]
4 [1, 6, 9, 7, 5]
5 [1, 2, 3, 4, 0, 5]
5 [1, 6, 9, 4, 0, 5]
5 [1, 0, 4, 9, 7, 5]
5 [1, 0, 4, 3, 8, 5]
6 [1, 0, 4, 9, 6, 8, 5]
6 [1, 0, 4, 3, 2, 7, 5]
6 [1, 6, 9, 4, 3, 8, 5]
6 [1, 2, 3, 4, 9, 7, 5]
6 [1, 2, 7, 9, 6, 8, 5]
6 [1, 6, 8, 3, 2, 7, 5]
6 [1, 2, 7, 9, 4, 0, 5]
6 [1, 6, 8, 3, 4, 0, 5]
7 [1, 2, 3, 8, 6, 9, 7, 5]
7 [1, 6, 8, 3, 4, 9, 7, 5]
7 [1, 2, 7, 9, 4, 3, 8, 5]
7 [1, 6, 9, 7, 2, 3, 8, 5]
7 [1, 6, 9, 4, 3, 2, 7, 5]
7 [1, 2, 3, 4, 9, 6, 8, 5]
8 [1, 0, 4, 3, 8, 6, 9, 7, 5]
8 [1, 2, 3, 8, 6, 9, 4, 0, 5]
8 [1, 0, 4, 9, 7, 2, 3, 8, 5]
8 [1, 6, 9, 7, 2, 3, 4, 0, 5]
9 [1, 0, 4, 3, 2, 7, 9, 6, 8, 5]
9 [1, 0, 4, 9, 6, 8, 3, 2, 7, 5]
9 [1, 2, 7, 9, 6, 8, 3, 4, 0, 5]
9 [1, 6, 8, 3, 2, 7, 9, 4, 0, 5]
```


## Some other implementations:

* In [NetworkX](https://networkx.github.io), method [shortest_simple_paths](https://networkx.github.io/documentation/stable/_modules/networkx/algorithms/simple_paths.html) implements in Python the Yen's algorithm [9]
* In [JGraphT](https://jgrapht.org), method [YenKShortestPath](https://jgrapht.org/javadoc/org/jgrapht/alg/shortestpath/YenKShortestPath.html) implements in Java the algorithm described in [7].
  In [JGraphT](https://jgrapht.org) also offers an implementation of [Eppstein k shortest paths algorithm](https://jgrapht.org/javadoc/org/jgrapht/alg/shortestpath/EppsteinKShortestPath.html) [3]
* Implementation in Python for [NetworkX](https://networkx.github.io) of the algorithm described in [7]: [k-shortest-path](https://github.com/dsaidgovsg/k-shortest-path)
* Implementations in C++, Jaca, Scala, etc. of the algorithm proposed in [7] can be found [here](http://thinkingscale.com/k-shortest-paths-cpp-version/) (warning: [possible memory leak](https://stackoverflow.com/questions/6709066/c-k-shortest-paths-algorithm))


## References
1. D. Ajwani, E. Duriakova, N. Hurley, U. Meyer and A. Schickedanz. An Empirical Comparison of k-Shortest Simple Path Algorithms on Multicores. In Proceedings of the 47th International Conference on Parallel Processing (ICPP 2018), pages 78:1--78:12, ACM, 2018. DOI:10.1145/3225058.3225075
2. A. Al Zoobi, D. Coudert, N. Nisse. **[Space and Time Trade-Off for the k Shortest Simple Paths Problem](http://dx.doi.org/10.4230/LIPIcs.SEA.2020.18)]**. 18th International Symposium on Experimental Algorithms (SEA), Jun 2020, Catania, Italy. pp.13 **[pdf](https://hal.inria.fr/hal-02865918/file/LIPIcs-SEA-2020-18.pdf)** [bib](https://hal.inria.fr/hal-02865918/bibtex)
3. D. Eppstein. "Finding the k Shortest Paths" (PDF). SIAM J. Comput. 28 (2): 652–673, 1998. doi:10.1137/S0097539795290477.
4. G. Feng. Finding k shortest simple paths in directed graphs: A node classification algorithm. Networks, 64(1):6–17, 2014. doi:10.1002/net.21552
5. J. Hershberger, M. Maxel, S. Suri. Finding the k shortest simple paths: A new algorithm and its implementation. ACM Transactions on Algorithms (TALG), 3:4.45, November 2007. DOI:10.1145/1290672.1290682
6. D. Kurz and P. Mutzel. A sidetrack-based algorithm for finding the k shortest simple paths in a directed graph. In International Symposium on Algorithms and Computation (ISAAC), volume 64 of LIPIcs, pages 49:1–49:13. Schloss Dagstuhl - Leibniz-Zentrum fuer Informatik, December 2016.doi:10.4230/LIPIcs.ISAAC.2016.49.
7. Q. V. Martins, Ernesto and M. B. Pascoal, Marta. (2003). A new implementation of Yen's ranking loopless paths algorithm. Quarterly Journal of the Belgian, French and Italian Operations Research Societies. 1. 121-133. DOI:10.1007/s10288-002-0010-2
8. J. Y. Yen. Finding the K shortest loopless paths in a network. Management Science, 17:712-716, 1971.
9. J. Y. Yen. Another algorithm for finding the K shortest loopless network paths. In Proc. of 41st Mtg. Operations Research Society of America, volume 20, 1972.
