"""
Interface for Dijkstra
"""
# ****************************************************************************
# Copyright (C) 2021 Ali Al Zoobi <ali.al-zoobi@inria.fr>
#                    David Coudert <david.coudert@inria.fr>
#                    Nicolas Nisse <nicolas.nisse@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/
# ****************************************************************************

from libcpp.pair cimport pair
from libcpp.vector cimport vector
from libcpp.set cimport set as cpp_set
from libcpp.unordered_set cimport unordered_set as cpp_unordered_set

from sagemath.types cimport vertex_t, weight_t

from sagemath.digraph cimport DirectedGraph

cdef extern from "../include/BFM.h":
    pass

# Declare the class with cdef
cdef extern from "../include/BFM.h" namespace "BFM":
    cdef cppclass BFM[TI, TV]:
        BFM()
        BFM(DirectedGraph[TI,TV] *, TI, bint) except +
        BFM(DirectedGraph[TI,TV] *, cpp_unordered_set[TI], cpp_set[pair[TI,TI]], TI, bint) except +
        bint run()
        TV weight(TI)
        TI predecessor(TI)
        TI successor(TI)
        vector[TI] get_path(TI)
        bint hasNegativeCycle()

ctypedef BFM[vertex_t,weight_t] StandardBFM
