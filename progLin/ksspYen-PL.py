
import MyFunctions
from operator import itemgetter


class ksspYenPL:
    g = DiGraph()
    s = 0
    t = 0
    Old_Paths = []
    LastPath = []
    
    def __init__(self,gg,ss,tt):
        self.g = gg
        self.s = ss
        self.t = tt
        
        
    def __iter__(self):
        self.LastPath = self.new_shortest_path_pl()
        self.Old_Paths.insert(0,self.LastPath)
        return self
        # return self.Old_Paths[len(self.Old_Paths)-1]

    def next(self):
        self.LastPath = self.Yen_iterator()
        self.Old_Paths.insert(len(self.Old_Paths)-1,self.LastPath)
        return  self.LastPath
    

    def new_shortest_path_pl(self,removed_edges=[]):
        """ finding a shortest path from s to t  that is different than every path in Old_Paths """
        s = self.s
        t = self.t
        g = self.g
        Old_Paths = self.Old_Paths
        p = MixedIntegerLinearProgram(maximization=False)

        
        # ---------- Constraint 1 ------------------
        # ** Shortest path constraints using flow **
        #___________________________________________
        f = p.new_variable(binary=True) # f is the flow variable
        # one unit of flow going from s and entering t
        p.add_constraint(p.sum(f[(s,v)] for v in g.neighbor_out_iterator(s)) == 1)
        p.add_constraint(p.sum(f[(u,t)] for u in g.neighbor_in_iterator(t)) == 1)
        #flow conservation constraint
        for v in g:
            if v != s and v != t:
                p.add_constraint(p.sum(f[(u,v)] for u in g.neighbor_in_iterator(v)) == p.sum(f[(v,u)] for u in g.neighbor_out_iterator(v)))
                
        # ---------- Constraint 0 ------------------
        # ** removing the edges in removed_edges **
        #___________________________________________
        
        for e in removed_edges:
            p.add_constraint(f[(e[0],e[1])] <= 0)

        # ---------- Constraint 4 ------------------
        # ********** tour elimination **************
        #___________________________________________
        tl = p.new_variable() # tl stands for tour elimination
        n = g.order()
        for u,v,w in g.edge_iterator():
            p.add_constraint( tl[v] >= tl[u] + 1 + n*(f[(u,v)] - 1) )
            
            
        #-------- Objective function + Output -------
        # the objective function
        p.set_objective(sum(f[(u,v)]*w for (u,v,w) in g.edge_iterator()))
        try:
            Sol = p.solve()
            f_sol = p.get_values(f)
            D = DiGraph([e for e in f_sol if f_sol[e] == 1])
            Q = D.shortest_path(s, t)
            return  Q
        except:
            return []




    def Yen_iterator(self,max_k = 3):
        G = self.g
        node_start = self.s
        node_end = self.t
        SP = self.new_shortest_path_pl([])
        Dis = weight_in_graph(SP,G)
        if Dis == 0:
            return 0
        A = [{'cost': Dis,
              'path': SP,
              'dev-idx': 0,
             }]
        B = []
        removed_edges = []
        if not A[0]['path']: return A
        for k in range(1, max_k):
            #print 'ii'
            IP = A[-1]
            # print "Iterated_Path :",IP
            # print " "
            dev_idx = A[-1]['dev-idx']
            for i in range(dev_idx, len(A[-1]['path']) - 1):
                node_spur = A[-1]['path'][i]
                path_root = A[-1]['path'][:i+1]
                w_pref = weight_in_graph(path_root,G)

                # removing the links leading to old paths
                for path_k in A:
                    curr_path = path_k['path']
                    if len(curr_path) > i and path_root == curr_path[:i+1]:
                        if(G.has_edge(node_spur,curr_path[i+1])):
                            removed_edges.append([node_spur, curr_path[i+1]])

               # removing the prefix path in order to get only simple paths
                L = path_root
                for i in range(len(L)-1):
                    for v in G.neighbors_out(L[i]):
                        removed_edges.append([L[i], v])
                    for u in G.neighbors_in(L[i]):
                        removed_edges.append([u,L[i]])
                Spur_kssp = ksspYenPL(G,node_spur,node_end)
                path_spur = Spur_kssp.new_shortest_path_pl(removed_edges)


                # restoring the original graph
                removed_edges= []
                Dis = weight_in_graph(path_spur,G)

                if(Dis>0):
                    path_total = path_root[:-1] + path_spur
                    dist_total = w_pref + Dis
                    potential_k = {'cost': dist_total, 'path': path_total, 'dev-idx': i}
                    if not (potential_k in B):
                        #print "added path",potential_k
                        B.append(potential_k)


            if len(B):
                B = sorted(B, key=itemgetter('cost'))
                self.Old_Paths.append(B[0])
                print "B",B[0]
                B.pop(0)
        return B







"""
def kssp_yen(G, node_start, node_end, max_k=3):
    SP = G.shortest_path(node_start,node_end,by_weight=True)
    Dis = weight_in_graph(SP,G)
    #distances, previous = dijkstra(G, node_start)

    if Dis == 0:
        return 0
    A = [{'cost': Dis,
          'path': SP,
          'dev-idx': 0,
         }]
    B = []
    removed_edges = []
    if not A[0]['path']: return A
    for k in range(1, max_k):
        #print 'ii'
        IP = A[-1]
        print "Iterated_Path :",IP
        print " "
        dev_idx = A[-1]['dev-idx']
        for i in range(dev_idx, len(A[-1]['path']) - 1):
            node_spur = A[-1]['path'][i]
            path_root = A[-1]['path'][:i+1]
            w_pref = weight_in_graph(path_root,G)

            # removing the links leading to old paths
            for path_k in A:
                curr_path = path_k['path']
                if len(curr_path) > i and path_root == curr_path[:i+1]:
                    if(G.has_edge(node_spur,curr_path[i+1])):
                        removed_edges.append([node_spur, curr_path[i+1]])

           # removing the prefix path in order to get only simple paths
            L = path_root
            for i in range(len(L)-1):
                for v in G.neighbors_out(L[i]):
                    removed_edges.append([L[i], v])
                for u in G.neighbors_in(L[i]):
                    removed_edges.append([u,L[i]])
            Spur_kssp = ksspYenPL(G,node_spur,node_end)
            path_spur = Spur_kssp.new_shortest_path_pl(removed_edges)


            # restoring the original graph
            removed_edges= []
            Dis = weight_in_graph(path_spur,G)

            if(Dis>0):
                path_total = path_root[:-1] + path_spur
                dist_total = w_pref + Dis
                potential_k = {'cost': dist_total, 'path': path_total, 'dev-idx': i}
                if not (potential_k in B):
                    #print "added path",potential_k
                    B.append(potential_k)


        if len(B):
            B = sorted(B, key=itemgetter('cost'))
            A.append(B[0])
            #print "B",B[0]
            B.pop(0)
    return A

"""

#___________________________________________________________________________



def test_kssp(Filepath):
    D = readGraph(Filepath)
    s = 200
    t = 500
    D = pretraitement(D,s,t)
    #BB = kssp_yen(D, s, t, 3)
    # print "BB",BB
    #return 1
    MykSSP = ksspYenPL(D,s,t)
    MykSSP.Yen_iterator(3)
    return 1
    start = timer()
    k = 3
    i = 0
    MyIter = iter(MykSSP)
    for x in MyIter:
        i = i + 1
        print x
        if (i == k):
            break
    end = timer()
    print "iterative time :",end - start
    start = timer()
    all_together = MykSSP.k_shortest_path_pl(k)
    for path in all_together:
        print path
    end = timer()
    print "all-together time :",end - start


test_kssp("./graphs/Rome99.gr")










