"""
This file implements several ILPs for the k-shortest simple path problem in 
"""
from sage.numerical.mip import MixedIntegerLinearProgram, MIPSolverException
from sage.graphs.digraph import DiGraph
from sage.graphs.graph import Graph
from sage.rings.infinity import Infinity
from sage.rings.integer_ring import ZZ
from sage.misc.misc import walltime
import itertools


class ksspILP:
    """
    This class implements several ILP models for the k-shortest simple paths
    problem with arbitrary arc weights.
    """

    def __init__(self, G, source, target, k, model='MTZ inc',
                     solver=None, solver_verbose=0, verbose=0,
                     timelimit=None, logfile=None, nthreads=None):
        """
        Constructor

        INPUT:

        - ``G`` -- a weighted DiGraph

        - ``source, target`` -- the source and target of the paths

        - ``k``-- integer; the number of paths to compute

        - ``model`` -- string (default: ``'MTZ inc'``); specify the name of the
          model to use. The first keyword is the type of subtour elimination
          constraints, and the second indicates whether the paths are computed
          one after the other (``'inc'``, faster) or all at once (``'all'``,
          slower).

          The possible subtour elimination constraints are:

            - ``'MTZ'`` -- Miller-Tucker-Zemlin subtour elimination constraints,
              with the addition of strengthening constraints proposed by
              Desrochers and Laporte, and constraints based on BFS distances.

            - ``'SF'`` -- Single auxiliary flow formulation, as proposed by
              Gavish and Graves.

            - ``'GCS'`` -- Generalized Cutset Inequalities, proposed by
              Fischetti et al. (1998), to strenghten the classical
              Dantzig-Fulkerson-Johnson cutset based formulation. The separation
              of violated inequalities is done using strongly-connected
              components.

            - ``'GCS*'`` -- (NOT IMPLEMENTED YET) a variant of ``'GCS'`` in
              which we start solving the relaxed version, and then proceed with
              the integral version.

        - ``solver`` -- (default: ``None``); specify a Linear Program (LP)
          solver to be used. If set to ``None``, the default one is used. For
          more information on LP solvers and which default solver is used, see
          the method :meth:`sage.numerical.mip.MixedIntegerLinearProgram.solve`
          of the class :class:`sage.numerical.mip.MixedIntegerLinearProgram`.

        - ``solver_verbose`` -- integer (default: ``0``); sets the level of
          verbosity of the LP solver. Set to 0 by default, which means quiet.

        - ``verbose`` -- integer (default: ``0``); sets the level of verbosity
          (except the LP solver). Set to 0 by default, which means quiet.

        - ``timelimit`` -- (default: ``None``); specify a computation time limit
          in seconds (unlimited by defaut). If the problem is not solved before
          the time limit, no solution is returned.

        - ``logfile`` -- string (default: ``None``); specify the name of the
          logfile of the LP solver.

        - ``nthreads`` -- (default: ``None``); specify the maximum number of
          threads used by LP solvers. Works only for solvers accepting this
          option (e.g., ``Cplex``).
        """
        self.start_time = walltime()

        if not isinstance(G, DiGraph):
            raise ValueError("the first input parameter must be a DiGraph")
        if source not in G:
            raise ValueError("the source must be in the digraph")
        if target not in G:
            raise ValueError("the target must be in the digraph")
        if k < 1:
            raise ValueError("parameter k must be at least 1")

        # Check the model
        tmp = model.strip().split()
        if tmp[0] in ['GCS', 'MTZ', 'SF']:
            self.subtour_model = tmp[0]
        else:
            raise ValueError("unrecognized model '{}'".format(model))
        if tmp[1] in ['all', 'inc']:
            self.incremental_model = tmp[1] == 'inc'
        else:
            raise ValueError("unrecognized model '{}'".format(model))

        self.G = G
        self.source = source
        self.target = target
        self.model = model
        self.solver = None if solver is None else solver.upper()
        self.verbose = verbose
        self.solver_verbose = solver_verbose
        self.timelimit = timelimit
        self.logfile = logfile
        self.nthreads = nthreads
        self.constraint_generation = self.incremental_model or model.startswith('GCS')

        self.objective_value = +Infinity
        self.paths = []
        self.counter = {'GCS': 0}

        # Trick to deal with both incremental (one commodity at a time) and non
        # incremental (all commodities at once) models
        self.commodities = [i for i in range(1 if self.incremental_model else k)]

        
        self.init_mip()

        if self.incremental_model:
            for i in range(k):
                if self.subtour_model == 'GCS':
                    self.solve_branch_and_cut()
                else:
                    self.solve_compact()

                if self.objective_value == +Infinity:
                    break

                self.extract_solution()  # a single path, if found

        else:
            if self.subtour_model == 'GCS':
                self.solve_branch_and_cut()
            else:
                self.solve_compact()

            self.extract_solution()  # k paths, if found
                
        self.stop_time = walltime()
        

    def __dealloc__(self):
        """
        Destructor
        """

    def __repr__(self):
        """
        Return a string representation of self.
        """
        st = self.model
        if self.objective_value == +Infinity:
            st += "\t--"
        else:
            st += '\t{tt:4.2f}'.format(tt=self.stop_time - self.start_time)
        if self.subtour_model == 'GCS':
            st += '\t{}'.format(self.counter['GCS'])
        else:
            st += '\t--'
        return st


        

    def init_mip(self):
        """
        Initialize the model.

        Define the variables, add the constraints, and set the objective.
        """
        mip = MixedIntegerLinearProgram(maximization=False, solver=self.solver,
                                            constraint_generation=self.constraint_generation)
        self.mip = mip

        # Set solver parameters
        if self.timelimit is not None:
            mip.solver_parameter("timelimit", self.timelimit)
        if self.logfile is not None:
            mip.solver_parameter("logfile", self.logfile)
        if self.nthreads is not None and self.solver == 'CPLEX':
            mip.solver_parameter("CPX_PARAM_THREADS", self.nthreads)

        self.variables = dict()

        #
        # x for selected edges -- s-t path
        #
        x = mip.new_variable(binary=True, name="x")
        self.variables['x'] = x
        self.xflow_in = lambda c, u: mip.sum(x[c, e] for e in self.G.incoming_edge_iterator(u, labels=False))
        self.xflow_out = lambda c, u: mip.sum(x[c, e] for e in self.G.outgoing_edge_iterator(u, labels=False))

        for c in self.commodities:
            mip.add_constraint(self.xflow_in(c, self.source) == 0)
            mip.add_constraint(self.xflow_out(c, self.target) == 0)
            for u in self.G:
                mip.add_constraint(self.xflow_in(c, u) - self.xflow_out(c, u)
                                    == ((-1) if u == self.source else (1 if u == self.target else 0)))
            for u in self.G:
                mip.add_constraint(self.xflow_in(c, u) <= 1)

        # Set objective
        mip.set_objective(mip.sum(w * mip.sum(x[c, (u, v)] for c in self.commodities)
                                      for u, v, w in self.G.edges()))


        #
        # the paths must be differents
        #
        if not self.incremental_model:
            y =  mip.new_variable(binary=True, name="x")
            self.variables['y'] = y

            for i, j in itertools.combinations(self.commodities, 2):
                for e in self.G.edges(labels=False):
                    mip.add_constraint(y[i, j, e] <= x[i, e])
                    mip.add_constraint(y[i, j, e] <= x[j, e])
                    mip.add_constraint(x[i, e] + x[j, e] <= 1 + y[i, j, e])

                mip.add_constraint(1 + mip.sum(y[i, j, e] for e in self.G.edges(labels=False))
                                       <= mip.sum(x[i, e] for e in self.G.edges(labels=False)))
                mip.add_constraint(1 + mip.sum(y[i, j, e] for e in self.G.edges(labels=False))
                                       <= mip.sum(x[j, e] for e in self.G.edges(labels=False)))
            
        
        if self.subtour_model == 'MTZ':
            self.add_MTZ_contraints()
        elif self.subtour_model == 'SF':
            self.add_SF_contraints()


    def add_MTZ_contraints(self, bfs=True):
        """
        Add variables and constraints for the MTZ subtour elimination constraints
        """
        mip = self.mip
        x = self.variables['x']
        n = self.G.order()

        # p for relative position
        p = mip.new_variable(nonnegative=True, name="p")
        self.variables['p'] = p

        # MTZ constraints
        for u, v in self.G.edges(labels=False):
            for c in self.commodities:
                mip.add_constraint(p[c, u] + 1 + n * (x[c, (u, v)] - 1) <= p[c, v])

        # DL constraints
        for u, v in self.G.edges(labels=False):
            if self.G.has_edge(v, u):
                for c in self.commodities:
                    mip.add_constraint(p[c, u] - p[c, v] + (n - 1)*x[c, (u, v)] + (n - 3)*x[c, (v, u)] <= n - 2)

        # BFS constraints
        if bfs:
            BFS = dict(self.G.breadth_first_search(self.source, report_distance=True)) 
            for c in self.commodities:
                for u in self.G:
                    mip.add_constraint(BFS[u] <= p[c, u])


    def add_SF_contraints(self):
        """
        Add variables and constraints for the SF subtour elimination constraints
        """
        mip = self.mip
        x = self.variables['x']
        n = self.G.order()

        #
        # z for selected vertices
        #
        z = mip.new_variable(binary=True, name="z")
        self.variables['z'] = z

        for c in self.commodities:
            # the target is selected, not the source
            mip.add_constraint(z[c, self.source] == 0)
            mip.add_constraint(z[c, self.target] == 1)

            # Each selected vertex, except the source, has one incoming arc.
            # Each selected vertex, except the target, has an outgoing arc.
            # A vertex, except the source, is selected if it has outgoing arcs.
            for u in self.G:
                mip.add_constraint(self.xflow_in(c, u) == z[c, u])
                if u != self.target:
                    mip.add_constraint(self.xflow_out(c, u) >= z[c, u])
                if u != self.source:
                    for e in self.G.outgoing_edge_iterator(u, labels=False):
                        mip.add_constraint(x[c, e] <= z[c, u])

        #
        # q for auxiliary flow variables
        #
        q = mip.new_variable(nonnegative=True, name="q")
        self.variables['q'] = q
        self.qflow_in = lambda c, u: mip.sum(q[c, e] for e in self.G.incoming_edge_iterator(u, labels=False))
        self.qflow_out = lambda c, u: mip.sum(q[c, e] for e in self.G.outgoing_edge_iterator(u, labels=False))

        for c in self.commodities:
            # The q-flow uses only selected edges
            for e in self.G.edges(labels=False):
                mip.add_constraint(q[c, e] <= (n - 1) * x[c, e])

            # Flow conservation constraints
            for u in self.G:
                if u == self.source:
                    mip.add_constraint(self.qflow_in(c, self.source) == 0)
                    # The source sends 1 unit of flow per selected vertex
                    mip.add_constraint(self.qflow_out(c, self.source) == mip.sum(z[c, v] for v in self.G))
                else:
                    # Each selected vertex collects 1 unit of flow
                    mip.add_constraint(self.qflow_in(c, u) - self.qflow_out(c, u) == z[c, u])



    def solve_compact(self):
        """
        Solve a compact MIP (e.g., MTZ of SF models).
        """
        mip = self.mip

        try:
            self.objective_value = mip.solve(log=self.solver_verbose)

            if ( (self.timelimit is not None) and
                ( (mip.solver_parameter("timelimit") > self.timelimit) or
                (walltime() - self.start_time > self.timelimit))):
                self.objective_value = +Infinity
        except MIPSolverException:
            self.objective_value = +Infinity


    def solve_branch_and_cut(self):
        """
        Solve using branch-and-cut (i.e., constraints generation).
        """
        mip = self.mip
        x = self.variables['x']

        while True:

            self.solve_compact()

            if self.objective_value == +Infinity:
                # Either no solution or timelimit
                break

            x_val = mip.get_values(x)
            self._x_val = x_val

            # We build the digraphs of selected arcs
            DD = [DiGraph() for _ in self.commodities]
            for (c, e), val in x_val.items():
                if val > 0:
                    DD[c].add_edge(e)

            cpt = 0
            for c in self.commodities:
                D = DD[c]
                # Remove source and target from D
                D.delete_vertex(self.source)  # should not be in...
                D.delete_vertex(self.target)  # should not be in...
                if D.order() < 2:
                    continue

                for scc in D.strongly_connected_components():
                    if len(scc) <= 1:
                        continue
                    # get the arcs going out of the scc
                    ES = self.G.edge_boundary(scc, labels=False)
                    S_out = sum(x_val[c, e] for e in ES)  # should be 0
                    # and add a constraint per vertex in scc
                    for v in scc:
                        Ev = self.G.outgoing_edges(v, labels=False)
                        v_out = sum(x_val[c, e] for e in Ev)
                        if S_out < v_out:
                            # the constraint is violated
                            mip.add_constraint(mip.sum(x[c, e] for e in Ev) <= mip.sum(x[c, e] for e in ES))
                            cpt += 1

            if cpt:
                self.counter['GCS'] += cpt
            else:
                # We have not added any constraint, so we cannot improve.
                break


    def extract_solution(self, add_constraint=True):
        """
        Extract path(s) from the solution.

        For each newly found path, we add a tuple ``(cost, path, time)`` to the
        list of all found paths, where ``cost`` is the length of the path,
        ``path`` is the non-ordered list of edges of the path, and ``time`` is
        the computing time since the begining.
        """
        if self.objective_value == +Infinity:
            return

        x = self.variables['x']
        x_val = self.mip.get_values(x)
        new_paths = [[] for _ in self.commodities]
        for (c, e), val in x_val.items():
            if val > 0:
                new_paths[c].append(e)

        for c, path in zip(self.commodities, new_paths):
            obj = sum(self.G.edge_label(u, v) for u, v in path)
            time = walltime() - self.start_time
            self.paths.append((obj, path, time))
            if self.verbose == 1:
                print(obj, '{t:4.2f}'.format(t=time)) # path)
            elif self.verbose == 2:
                print(obj, '{t:4.2f}'.format(t=time), path)
            self.mip.add_constraint(self.mip.sum(x[c, e] for e in path) <= len(path) - 1)





def test_my_class(G, source, target, k, verbose=0, models=None):
    """
    """
    if models is None:
        models = ['MTZ inc', 'SF inc', 'GCS inc']
        if G.order() * k < 500:
            models.extend(['MTZ all', 'SF all', 'GCS all'])

    for model in models:
        print(ksspILP(G, source, target, k, model=model, verbose=verbose))


def test_grid(n, m, k, verbose=0):
    source = 0
    target = n * m -1
    verbose = 0

    G = DiGraph(graphs.Grid2dGraph(n, m))
    G.relabel(inplace=True)
    for u, v in G.edges(labels=False):
        G.set_edge_label(u, v, -1)

    test_my_class(G, source, target, k, verbose=verbose, models=['MTZ inc', 'SF inc', 'GCS inc'])
