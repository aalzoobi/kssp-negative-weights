import networkx
#from prioritydictionary import priorityDictionary
import random
import timeit
from timeit import default_timer as timer

def readGraph(path):
    D = DiGraph()
    with open(path,'r') as f:
        for line in f:
            if line.startswith('#'):
                continue
            #print "line",line
            #s,d,l = line.split()
            bla,s,d,l = line.split()
            D.add_edge(int(s),int(d),float(l))
        #print s,d,l
    return D


def pretraitement(g,s,t): # delete useless edges
    N_in_s = g.neighbors_in(s)
    for e in N_in_s:
        g.delete_edge(e,s)

    N_out_t = g.neighbors_out(t)
    for e in N_out_t:
        g.delete_edge(t,e)

    return g



def string_to_list(P):
    L = []
    for i in P:
        L.append(int(i))
    return L
    
    
""" In order to transform the type of a path from a list of vertices
	to a list of edges (u,v,cost) we define this function called
	vertices to edges """
def vertices_to_edges(P,G):
# where P is a list of vertices and G is the graph where it belongs
    L = []
    for i in range(len(P)-1):
        L.append([P[i],P[i+1],G.edge_label(P[i], P[i+1])])
    return(L)



def path_to_edges(P):
    # where P is a list of vertices and G is the graph where it belongs
    L = []
    for i in range(len(P)-1):
        L.append([P[i],P[i+1]])
    return(L)



""" weight of one argument takes as input a path (as a list of edges)
    and returns its weight. Note that there is no need to add the graph
    where the path belongs as long as the edge weight is included in the list """

def weight(P):
    return sum(P[i][2] for i in range(len(P)))



def weight_in_graph(P,G): # here we consider P as a list of vertices
    sum = 0
    for i in range(len(P)-1):
        sum = sum + G.edge_label(P[i], P[i+1])
    return(sum)




# grilles(poids egale 1 100*100), graphes aleatoire, paires aleatoire avec time limite,plot, New york

