

import MyFunctions
import timeit
from timeit import default_timer as timer


def find_all(g,s,t,k,Theta, verbose=0):  #  fing k dissimilar path using the Asy measure
    p = MixedIntegerLinearProgram(maximization=False)
    x = p.new_variable(binary=True)
    y = p.new_variable(binary=True)
    I = range(1,k+1)

    for i in I:
        print i
        p.add_constraint(p.sum(x[s,v,i] for v in g.neighbor_out_iterator(s)) == 1)
        p.add_constraint(p.sum(x[u,t,i] for u in g.neighbor_in_iterator(t)) == 1)
        for v in g:
            if v != s and v != t:
                p.add_constraint(p.sum(x[u,v,i] for u in g.neighbor_in_iterator(v)) == p.sum(x[v,u,i] for u in g.neighbor_out_iterator(v)))

        for j in I:
            if j != i:
                # linearizations ..
                for (u,v,w) in g.edge_iterator():
                    p.add_constraint(y[u,v,i,j] <= x[u,v,i])
                    p.add_constraint(y[u,v,i,j] <= x[u,v,j])
                    p.add_constraint(x[u,v,i] + x[u,v,j] <= y[u,v,i,j] + 1)

                # dissimilarity constraint (considering the Asymertric one)
                p.add_constraint(p.sum(w*y[u,v,i,j] for (u,v,w) in g.edge_iterator()) <= Theta * p.sum(w*x[u,v,i] for (u,v,w) in g.edge_iterator()))
    p.set_objective(p.sum(w * p.sum(x[u,v,i] for i in I) for (u,v,w) in g.edge_iterator()))

    # solving the program and getting the values
    Sol = p.solve(log=verbose)
    f_sol = p.get_values(x)

    # now, tracing the paths as they are the k shortest paths from s to $t$
    PathList = []
    OutPaths = []
    for i in I:
        for (u,v,w) in g.edge_iterator():
            if f_sol[u,v,i] == 1:
                PathList.append((u,v,w))

        D = DiGraph([e for e in PathList])
        Q = D.shortest_path(s, t)
        P = []
        for v in range(len(Q)-1):
            P.append([Q[v],Q[v+1],g.edge_label(Q[v],Q[v+1])])
        # print "P is ",P
        # print "sp",Q
        OutPaths.append(P)
        D = []
        PathList = []
    return (OutPaths)






def sim_Asy(P_1,P_2):
    w_P_1 = 0
    intersec = 0
    for e in P_1:
        w_P_1 = w_P_1 + e[2]
        if e in P_2:
            intersec = intersec + e[2]
    return intersec*1.0/w_P_1



def test(Path):
    D = readGraph(Path)
    s = 10
    t = 3000
    k = 5
    Theta = 0.7
    D = pretraitement(D,s,t)
    start = timer()
    Out = k_diss_sp_prog_lin(D,s,t,k,Theta)
    for path in Out:
        print "w :",weight_in_graph(path,D)," ",path
    end = timer()
    #print "bla bla",blabla
    print "with time :",end - start


test("./Rome")



