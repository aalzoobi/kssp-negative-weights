import MyFunctions
import timeit
from timeit import default_timer as timer




def shortest_path_pl(g,s,t):
    """ finding a shortest path from s to t in g using a classical shortest path linear program """
    p = MixedIntegerLinearProgram(maximization=False)
    f = p.new_variable(binary=True) # f is the flow variable

    # one unit of flow going from s and entering t
    p.add_constraint(p.sum(f[(s,v)] for v in g.neighbor_out_iterator(s)) == 1)
    p.add_constraint(p.sum(f[(u,t)] for u in g.neighbor_in_iterator(t)) == 1)

    #flow conservation constraint
    for v in g:
        if v != s and v != t:
            p.add_constraint(p.sum(f[(u,v)] for u in g.neighbor_in_iterator(v)) == p.sum(f[(v,u)] for u in g.neighbor_out_iterator(v)))

    # subtour elimination
    tl = p.new_variable() # tl stands for tour elimination
    M = 99999
    for e in g.edges():
        u = e[0]
        v = e[1]
        p.add_constraint( tl[v] >= tl[u] + 1 + M*(f[(u,v)] - 1) )
        
        
    # the objective function
    p.set_objective(sum(f[(u,v)]*w for (u,v,w) in g.edge_iterator()))

    Sol = p.solve()
    f_sol = p.get_values(f)
    D = DiGraph([e for e in f_sol if f_sol[e] == 1])
    Q = D.shortest_path(s, t)
    return  Q




def new_shortest_path_pl(g,s,t,Old_Paths):
    """ finding a shortest path from s to t  that is different than every path in Old_Paths """
    p = MixedIntegerLinearProgram(maximization=False)
    
    # ---------- Constraint 1 ------------------
    # ** Shortest path constraints using flow **
    #___________________________________________
    f = p.new_variable(binary=True) # f is the flow variable
    # one unit of flow going from s and entering t
    p.add_constraint(p.sum(f[(s,v)] for v in g.neighbor_out_iterator(s)) == 1)
    p.add_constraint(p.sum(f[(u,t)] for u in g.neighbor_in_iterator(t)) == 1)
    #flow conservation constraint
    for v in g:
        if v != s and v != t:
            p.add_constraint(p.sum(f[(u,v)] for u in g.neighbor_in_iterator(v)) == p.sum(f[(v,u)] for u in g.neighbor_out_iterator(v)))
            
    # ---------- Constraint 2 ------------------
    # ** The intersection with old paths "y" **
    #___________________________________________
    i = 0
    y = p.new_variable() # y_i indicates the intersection of the new path with the i^th path in Old_Paths
    for Path in Old_Paths:
        Path_Edges = path_to_edges(Path)
        print "path edges"
        for u,v,w in g.edges():
            arc_in = 0
            if((u,v) in Path_Edges):
                arc_in = 1
            # putting the valid values of y_i
            p.add_constraint( y[(i,u,v)] <= arc_in )                        # Cons 2.1
            p.add_constraint( y[(i,u,v)] <= f[(u,v)] )                      # Cons 2.2
            p.add_constraint( f[(u,v)] + arc_in <= y[(i,u,v)]  + 1 )        # Cons 2.3
        i = i + 1
        
    # ---------- Constraint 3 ------------------
    # ******* The resultin path is new  *******
    #___________________________________________
    i = 0
    for Path in Old_Paths:
        p.add_constraint(p.sum(y[(i,u,v)] for u,v,w in g.edges()) <=  p.sum(f[(v,u)] for u,v,w in g.edges()) - 5)
        i = i + 1
            
            
    # ---------- Constraint 4 ------------------
    # ********** tour elimination **************
    #___________________________________________
    tl = p.new_variable() # tl stands for tour elimination
    M = 999999999
    for e in g.edges():
        u = e[0]
        v = e[1]
        p.add_constraint( tl[v] >= tl[u] + 1 + M*(f[(u,v)] - 1) )
    #-------- Objective function + Output -------
    # the objective function
    p.set_objective(sum(f[(u,v)]*w for (u,v,w) in g.edge_iterator()))
    Sol = p.solve()
    f_sol = p.get_values(f)
    y_sol = p.get_values(y)
    D_y = DiGraph([e for e in y_sol if y_sol[e] == 1])
    # Q_y = D_y.shortest_path(s,t)
    print "printing the ys"
    for e in D_y:
        print e
    D = DiGraph([e for e in f_sol if f_sol[e] == 1])
    Q = D.shortest_path(s, t)
    #print "Sol",Sol
    return  Q








def test_sp(Filepath):
    D = readGraph(Filepath)
    s = 100
    t = 3000
    D = pretraitement(D,s,t)
    start = timer()
    P = shortest_path_pl(D,s,t)
    print "w :",weight_in_graph(P,D)," ",P
    end = timer()
    #print "bla bla",blabla
    print "with time :",end - start




def test_new_sp(Filepath):
    D = readGraph(Filepath)
    s = 100
    t = 3000
    D = pretraitement(D,s,t)
    start = timer()
    P = shortest_path_pl(D,s,t)
    P1 = new_shortest_path_pl(D,s,t,[P])
    print "w :",weight_in_graph(P,D)," ",P
    print "w :",weight_in_graph(P1,D)," ",P1
    end = timer()
    #print "bla bla",blabla
    print "with time :",end - start













def find_diss_to_list_of_paths_prog_lin(g,s,t,listPaths,Theta, verbose=0):  #  find a path that is dissimilar to each path P in listPaths
    p = MixedIntegerLinearProgram(maximization=False)
    f = p.new_variable(binary=True, nonnegative = True) # f is the flow variable

    # one unit of flow going from s and entering t
    p.add_constraint(p.sum(f[(s,v)] for v in g.neighbor_out_iterator(s)) == 1)
    #p.add_constraint(p.sum(f[(s,v)] for v in g.neighbor_in_iterator(s)) == 0)

    p.add_constraint(p.sum(f[(u,t)] for u in g.neighbor_in_iterator(t)) == 1)
    #p.add_constraint(p.sum(f[(u,t)] for u in g.neighbor_out_iterator(t)) == 0)


    #flow conservation constraint
    for v in g:
        if v != s and v != t:
            p.add_constraint(p.sum(f[(u,v)] for u in g.neighbor_in_iterator(v)) == p.sum(f[(v,u)] for u in g.neighbor_out_iterator(v)))

    # the dissimilarity constraint
    for P in listPaths:
        W_P = weight_in_graph(P, g)
        p.add_constraint(p.sum(f[(u,v)]*g.edge_label(u, v) for u, v in zip(P[:-1], P[1:])) <= Theta * W_P )

    # the objective function
    p.set_objective(sum(f[(u,v)]*w for (u,v,w) in g.edge_iterator()))
    Sol = p.solve(log=verbose)
    f_sol = p.get_values(f)
    D = DiGraph([e for e in f_sol if f_sol[e] == 1])
    Q = D.shortest_path(s, t)
    return Sol, Q





def k_diss_sp_prog_lin(g,s,t,max_k,Theta): # works only with the asymetrical measure
    SP = g.shortest_path(s,t,by_weight=True)
    Old_Paths = []
    Old_Paths.append(SP)
    for k in range(1,max_k):
        # print "k",k
        W, New_P = find_diss_to_list_of_paths_prog_lin(g,s,t,Old_Paths,Theta)
        # print "w :", ,"New_P",New_P,
        Old_Paths.append(New_P)
    return Old_Paths








def test(Path):
    D = readGraph(Path)
    s = 100
    t = 3000
    k = 3
    Theta = 0.8
    D = pretraitement(D,s,t)
    start = timer()
    Out = k_diss_sp_prog_lin(D,s,t,k,Theta)
    for path in Out:
        print "w :",weight_in_graph(path,D)," ",path
    end = timer()
    #print "bla bla",blabla
    print "with time :",end - start

test("./Rome99.gr")
#test("./NYdistances")

#test_wiki_prog_lin()
