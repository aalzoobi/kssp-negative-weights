
load("MyFunctions.py") 


class ksspPL:
    g = DiGraph()
    s = 0
    t = 0
    Old_Paths = []
    LastPath = []
    
    def __init__(self,gg,ss,tt):
        self.g = gg
        self.s = ss
        self.t = tt
        
        
    def __iter__(self):
        self.LastPath = self.new_shortest_path_pl()
        #self.Old_Paths.insert(0,self.LastPath)
        return self
        # return self.Old_Paths[len(self.Old_Paths)-1]

    def next(self):
        self.LastPath = self.new_shortest_path_pl()
        self.Old_Paths.insert(len(self.Old_Paths)-1,self.LastPath)
        return  self.LastPath
    

    def new_shortest_path_pl(self):
        """ finding a shortest path from s to t  that is different than every path in Old_Paths """
        s = self.s
        t = self.t
        g = self.g
        Old_Paths = self.Old_Paths #equiv to copy
        p = MixedIntegerLinearProgram(maximization=False)
        
        # ---------- Constraint 1 ------------------
        # ** Shortest path constraints using flow **
        #___________________________________________
        f = p.new_variable(binary=True) # f is the flow variable
        # one unit of flow going from s and entering t
        p.add_constraint(p.sum(f[(s,v)] for v in g.neighbor_out_iterator(s)) == 1)
        p.add_constraint(p.sum(f[(u,t)] for u in g.neighbor_in_iterator(t)) == 1)
        #flow conservation constraint
        for v in g:
            if v != s and v != t:
                p.add_constraint(p.sum(f[(u,v)] for u in g.neighbor_in_iterator(v)) == p.sum(f[(v,u)] for u in g.neighbor_out_iterator(v)))
                
        # ---------- Constraint 2 ------------------
        # ** The intersection with old paths "y" **
        #___________________________________________
        i = 0
        y = p.new_variable(binary = True) # y_i indicates the intersection of the new path with the i^th path in Old_Paths
        for Path in Old_Paths:
            Path_Edges = path_to_edges(Path)
            #print "path edges", Path_Edges
            for u,v,w in g.edge_iterator():
                arc_in = 0
                if([u,v] in Path_Edges):
                    arc_in = 1
                p.add_constraint( y[(i,u,v)] <= arc_in )                        # Cons 2.1
                p.add_constraint( y[(i,u,v)] <= f[(u,v)] )                      # Cons 2.2
                p.add_constraint( f[(u,v)] + arc_in <= y[(i,u,v)]  + 1 )        # Cons 2.3
            i = i + 1
            
        # ---------- Constraint 3 ------------------
        # ******* The resulting path is new  *******
        #___________________________________________
        
        i = 0
        for Path in Old_Paths:
            p.add_constraint(p.sum(y[(i,u,v)] for u,v,w in g.edge_iterator()) <=  p.sum(f[(u,v)] for u,v,w in g.edge_iterator()) - 1)
            i = i + 1
        
        # ---------- Constraint 4 ------------------
        # ********** tour elimination **************
        #___________________________________________
        tl = p.new_variable() # tl stands for tour elimination
        n = g.order()
        for u,v,w in g.edge_iterator():
            p.add_constraint( tl[v] >= tl[u] + 1 + n*(f[(u,v)] - 1) )
            
            
        #-------- Objective function + Output -------
        # the objective function
        p.set_objective(sum(f[(u,v)]*w for (u,v,w) in g.edge_iterator()))
        Sol = p.solve()
        f_sol = p.get_values(f)
        D = DiGraph([e for e in f_sol if f_sol[e] == 1])
        Q = D.shortest_path(s, t)
        return  Q


            
    def k_shortest_path_pl(self,k):
        """ finding k shortest s-t path [ALL TOGETHER] """
        s = self.s
        t = self.t
        g = self.g
        Old_Paths = self.Old_Paths
        I = range(k)
        
        p = MixedIntegerLinearProgram(maximization=False)
        
        # ---------- Constraint 1 ------------------
        # ** Shortest path constraints using flow **
        #___________________________________________
        f = p.new_variable(binary=True) # f is the flow variable
        # one unit of flow going from s and entering t
        for i in I:
            p.add_constraint(p.sum(f[(i,s,v)] for v in g.neighbor_out_iterator(s)) == 1)
            p.add_constraint(p.sum(f[(i,u,t)] for u in g.neighbor_in_iterator(t)) == 1)
            #flow conservation constraint
            for v in g:
                if v != s and v != t:
                    p.add_constraint(p.sum(f[(i,u,v)] for u in g.neighbor_in_iterator(v)) == p.sum(f[(i,v,u)] for u in g.neighbor_out_iterator(v)))
                
        # ---------- Constraint 2 ------------------
        # ** The intersection with old paths "y" **
        #___________________________________________
        y = p.new_variable(binary = True) # y_i indicates the intersection of the new path with the i^th path in Old_Paths

        for i in I:
            for j in I:
                if (i != j):
                    for u,v,w in g.edge_iterator():
                        p.add_constraint(y[(i,j,u,v)] <= f[(i,u,v)])            # cons 2.1
                        p.add_constraint(y[(i,j,u,v)] <= f[(j,u,v)])            # cons 2.2
                        p.add_constraint(f[(i,u,v)] + f[(j,u,v)] <= y[(i,j,u,v)] + 1)            # cons 2.3


        # ---------- Constraint 3 ------------------
        # ******* The resulting path is new  *******
        #___________________________________________
        
        for i in I:
            for j in range(i + 1,k):
                p.add_constraint(p.sum(y[(i,j,u,v)] for u,v,w in g.edge_iterator()) <=  p.sum(f[(i,u,v)] for u,v,w in g.edge_iterator()) - 1)
                p.add_constraint(p.sum(y[(i,j,u,v)] for u,v,w in g.edge_iterator()) <=  p.sum(f[(j,u,v)] for u,v,w in g.edge_iterator()) - 1)

        
        # ---------- Constraint 4 ------------------
        # ********** tour elimination **************
        #___________________________________________
        tl = p.new_variable() # tl stands for tour elimination
        n = g.order()
        for i in I:
            for u,v,w in g.edge_iterator():
                p.add_constraint( tl[v] >= tl[u] + 1 + n*(f[(i,u,v)] - 1) )
            
            
        #-------- Objective function + Output -------
        # the objective function
        p.set_objective(sum(f[(i,u,v)]*w for i in I for (u,v,w) in g.edge_iterator()))
        Sol = p.solve()
        f_sol = p.get_values(f)
        L_paths = []
        for i in I:
            # print "i is ",i
            D = DiGraph([(u,v,j) for (j,u,v) in f_sol if f_sol[(j,u,v)] == 1 and j == i])
            Q = D.shortest_path(s, t)
            # print Q
            L_paths.insert(i,Q)
        return  L_paths




#___________________________________________________________________________


def test_kssp(Filepath):
    D = readGraph(Filepath)
    s = 100
    t = 600
    D = pretraitement(D,s,t)
    MykSSP = ksspPL(D,s,t)
    start = timer()
    k = 3
    i = 0
    MyIter = iter(MykSSP)
    for P in MyIter:
        i = i + 1
        w = weight_in_graph(P,D)
        print w, P
        if (i == k):
            break
    end = timer()
    print "iterative time :",end - start
    start = timer()
    all_together = MykSSP.k_shortest_path_pl(k)
    for P in all_together:
        w = weight_in_graph(P,D)
        print w, P
    end = timer()
    print "all-together time :",end - start


#test_kssp("./graphs/BIOGRID-small.txt")
test_kssp("./graphs/Rome99.gr")










